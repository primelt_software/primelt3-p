The Meaning of Pressure for Primary Magmas: New Insights from PRIMELT3-P; Herzberg et al. (2022)

This GUI is an interactive program of the macro-enabled excel version of PRIMELT3-P, also known as MEGA (Herzberg et al., 2022). It calculates the pressure, temperature, and composition of primary magmas of basalts that formed by adiabatic decompression fractional melting of fertile mantle peridotite. The GUI uses a python script that is entirely based on the primary magma calculations in MEGA. However, there are subtle differences between the two versions, which result from a slightly different approach to find the primary magma solutions in the python script. 

The program requires a delimited text file as input file with the composition of the primitive basalt, the FeO/FeOT ratio, and the fractionation pressure in bars. The output is divided into two separate blocks for batch and AFM solutions, respectively, and can be exported to several formats or copied to the clipboard for pasting into any spreadsheet program. Other additional features include an FeO vs MgO plot for olivine fractionation lines and pressure-temperature estimations for each primary magma solution. 


----------------------
Input structure:

Accepted file extensions are .csv and .txt files. The next lines are examples of the input format for two different samples. Data delimiters could be comma, semicolon, or tab. Oxides are in weight percent (wt%), Fe2Fet is the ferrous to total iron ratio (i.e., Fe+2/ΣFe), and the fractionation pressure P is given in bars.

Examples:


Name	SiO2	TiO2	Al2O3	Cr2O3	FeO	MnO	MgO	CaO	Na2O	K2O	NiO	P2O5	Fe2Fet	P   
MKea	48.3	2.64	13.15	0.0	11.056	0.18	8.05	13.18	2.28	0.74	0.0	0.311	0.9	1   
Gorgona	46.54	0.7	11.9	0.25	11.21	0.18	17.82	10.09	1.11	0.04	0.1	0.05	0.9	1   


Or

Name,SiO2,TiO2,Al2O3,Cr2O3,FeO,MnO,MgO,CaO,Na2O,K2O,NiO,P2O5,Fe2Fet,P      
MKea,48.3,2.64,13.15,0.0,11.056,0.18,8.05,13.18,2.28,0.74,0.0,0.311,0.9,1          
Gorgona,46.54,0.7,11.9,0.25,11.21,0.18,17.82,10.09,1.11,0.04,0.1,0.05,0.9,1 

Or

Name;SiO2;TiO2;Al2O3;Cr2O3;FeO;MnO;MgO;CaO;Na2O;K2O;NiO;P2O5;Fe2Fet;P      
MKea;48.3;2.64;13.15;0.0;11.056;0.18;8.05;13.18;2.28;0.74;0.0;0.311;0.9;1      
Gorgona;46.54;0.7;11.9;0.25;11.21;0.18;17.82;10.09;1.11;0.04;0.1;0.05;0.9;1


----------------------
Output structure:


Solution: 	Y (Yes) or N (No) --- whether a solution was found or not
Input Error: 	PX (Pyroxenite source) / VT (Volatile Peridotite source)
P: 		Fractionation pressure in bars (the same given as input)
Xfo: 		Forsterite composition of olivine in equilibrium with primary magma solution
KD: 		Fe-Mg distribution coefficient between the primary magma and olivine in equilibrium
T Ol/Liq (°C): 	Olivine-Liquid temperature at the fractionation pressure	
F AFM: 		Melt fraction for accumulated fractional melting (AFM) solution. Not given in batch solution
F (Fe/Mg): 	Melt fraction F for batch melting solution. Not given in AFM solution
TP (°C): 	Mantle potential temperature	
Pi:		Initial melting pressure. Not given in batch solution
Pf:		Final melting pressure. Not given in batch solution	
T Pf (°C): 	Temperature at final melting pressure. Not given in batch solution	
Residue:		Harz (Harzburgite) / Sp-Per (Spinel Peridotite) / Grt-Per (Garnet Peridotite) --- Residual lithology for the primary magma solution. Not given in batch solution	
%Ol:		Olivine addition/subtraction required for primary magma solution (>0 for olivine fractionation; <0 for olivine accumulation)	

Primary magma composition in oxide (wt%):
SiO2	TiO2	Al2O3	Cr2O3	Fe2O3	FeO	MnO	MgO	CaO	Na2O	K2O	NiO	P2O5

Notes: 

These are the versions of PRIMELT3-P included with the submission of the original manuscript:

Microsoft Excel version of PRIMELT3-P (also known as MEGA)
PRIMELT3-P GUI for Windows (compiled in Windows 10, Intel Core i9, Python 3.9.15)
PRIMELT3-P GUI for macOS (Compiled in macOS Ventura 13.0.1, , Intel Core i7, Python 3.9.15)

The first time you run the GUI it may take some time to start. It has been tested on different Windows and MAcOS versions and should run well in machines using both Intel and M chips. If you experience any problems, cannot start the program, or need a new compilation that runs in older/newer versions of Windows/macOS, please visit Gitlab (https://gitlab.com/primelt_software/primelt3-p) or send an email to jdhernandez@caltech.edu.

If you have problems opening the Excel version due to Microsoft Office blocking macros after an update, please follow the following instructions:

1. Close the workbook.
2. Right-click on the workbook.
3. Select Properties.
4. Under the General tab, make sure to check the Unblock box in Security.
5. Hit the Apply button.
6. Now open the workbook.
7. You will see the error message has now disappeared.

There may be small misfits between the Excel version and the GUI. These differences result from the different strategies implemented in Excel and Python to find the melt fraction intersections. Of the samples we have tested, the differences in the MgO content of the primary magma solutions calculated in Excel and the GUI were in general less than 0.2% MgO, which leads to small variations in Pf and Tp.

